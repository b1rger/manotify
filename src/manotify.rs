mod register;
mod config;

extern crate clap;
extern crate xdg;
extern crate notify_rust;
extern crate mammut;
extern crate regex;
extern crate dbus;

use clap::{Arg, App};
use std::fs;
use std::time::Duration;
use std::{thread,process};
use std::path::PathBuf;
use mammut::Mastodon;
use regex::Regex;
use std::process::Command;
use dbus::{BusType, Connection, Message, MessageItem, MessageItemArray};

fn main() {
    let matches = App::new("manotify")
        .version("0.1")
        .about("Send desktop notification about new toots.")
        .author("Birger Schacht")
        .arg(Arg::with_name("config")
             .short("c")
             .long("config")
             .value_name("FILE")
             .takes_value(true)
             .help("Set a custom config file."))
        .arg(Arg::with_name("daemon")
             .short("d")
             .long("daemon")
             .help("Keep checking for new toots in a regular interval."))
        .arg(Arg::with_name("seconds")
             .short("s")
             .long("seconds")
             .help("Time in seconds to rest between checking for new toots.")
             .takes_value(true)
             .default_value("300"))
        .get_matches();


    // lets find out where to store the configuration
    let xdg_dirs = xdg::BaseDirectories::with_prefix("manotify")
        .expect("Could not find xdg base directories");

    // the config_path is either the default location
    // or the one passed using -c
    let mut config_path: PathBuf;
    match matches.value_of("config") {
        Some(i) => config_path = PathBuf::from(i),
        None => config_path = xdg_dirs.place_config_file("config.toml")
            .expect("Cannot create configuration directory")
    }

    // read the config or create a new one if it does
    // not exist
    let content = match fs::read_to_string(&config_path) {
        Ok(content) => content,
        Err(_) => String::new(),
    };
    // and parse the config
    let mut config: config::Config = toml::from_str(&content)
        .expect("Coudl not parse config file");

    // lets initialize our mastodon instance
    // if there is no data in the config file
    // try to register a new account
    let mut mastodon;
    if config.mammutdata.is_none() {
        mastodon = match register::register() {
            Ok(mastodon) => mastodon,
            Err(_e) => {
                println!("Well, this is embarrassing. Sorry, but I wasn't able to follow through with the registration! Are you sure you gave me the correct data?");
                process::exit(1);
            }
        };
        config.mammutdata = Some(mastodon.data.clone());
    } else {
        let mammutdata = config.mammutdata.clone();
        mastodon = Mastodon::from_data(mammutdata.unwrap());
    }
    
    // lets initialize the last_id we have seen
    if config.manotify.is_none() {
        let manotify = config::Manotify {
            last_id: 0,
        };
        config.manotify = Some(manotify);
    }

    // and write the config file once
    let toml = toml::to_string(&config)
        .expect("Could not convert config to string");
    fs::write(&config_path, toml.as_bytes())
        .expect("Error writing config file");
    

    // if the -d option was passed, start
    // a daemon that checks for toots every
    // n seconds. otherwise check once and
    // exit
    if matches.is_present("daemon") {
        loop{
            config.manotify = fetch_toots(&mut mastodon, config.manotify.unwrap());
            let toml = toml::to_string(&config)
                .expect("Could not convert config to string");
            fs::write(&config_path, toml.as_bytes())
                .expect("Error writing config file");
            let seconds: u64 = matches.value_of("seconds").unwrap().parse().unwrap();
            println!("Sleeping for {} seconds", seconds);
            thread::sleep(Duration::new(seconds, 0));
        }
    } else {
        config.manotify = fetch_toots(&mut mastodon, config.manotify.unwrap());
        let toml = toml::to_string(&config)
            .expect("Could not convert config to string");
        fs::write(&config_path, toml.as_bytes())
                .expect("Error writing config file");
    }
}

// fetch the new toots and return a manotify config snipplet
fn fetch_toots(mastodon: &Mastodon, manotify: config::Manotify) -> Option<config::Manotify> {
    let timeline;
    let mut manotify: config::Manotify = manotify;
    match mastodon.get_home_timeline() {
        Ok(v) => {
            timeline = v;
            let mut items = timeline.initial_items;
            items.reverse();
            for status in items {
                let sid = status.id.parse::<u64>().unwrap();
                let body = sanitize(&status.content);
                if sid > manotify.last_id {
                    notify(&status.account.acct, &body, 10000);
                    manotify.last_id = sid;
                }
            }
        },
        Err(_) => println!("Could not fetch home timeline."),
    }
    Some(manotify)
}

// sanitize the html output
fn sanitize(html: &std::string::String) -> std::string::String {
    let ahref = Regex::new(r"<a([^>]+)>(?P<content>.*?)</a>").unwrap();
    let span = Regex::new(r"<span([^>]+)>(?P<content>.*?)</span>").unwrap();
    let mut newhtml = str::replace(html, "<p>", "");
    newhtml = str::replace(&newhtml, "</p>", "\n");
    newhtml = str::replace(&newhtml, "<br>", "\n");
    newhtml = str::replace(&newhtml, "<br />", "\n");
    newhtml = ahref.replace_all(&newhtml, "$content").to_string();
    newhtml = span.replace_all(&newhtml, "$content").to_string();
    newhtml
}

// open an URI with the default browser
// not used yet.
#[allow(dead_code)]
fn openuri(uri: std::string::String) {
    Command::new("xdg-open")
        .arg(uri)
        .spawn()
        .expect("Could not open URI.");
}

fn notify(summary: &std::string::String, message: &std::string::String, timeout: i32) {
    let c = Connection::get_private(BusType::Session).unwrap();
    let mut m = Message::new_method_call("org.freedesktop.Notifications", "/org/freedesktop/Notifications", "org.freedesktop.Notifications", "Notify").unwrap();

    m.append_items(&[
                   "Manotify".to_owned().into(), // appname
                   (0 as u32).into(), // notification to update
                   "".to_owned().into(),      // icon
                   summary.to_owned().into(), // summary (title)
                   message.to_owned().into(),      // body
                   MessageItem::Array(MessageItemArray::new(vec![], "as".into()).unwrap()),
                   MessageItem::Array(MessageItemArray::new(vec![], "a{sv}".into()).unwrap()),
                   timeout.into()                 // timeout
    ]);
    c.send_with_reply_and_block(m, 1000).unwrap();
}
