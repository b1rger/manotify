use serde::{Serialize, Deserialize};

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub mammutdata: Option<mammut::Data>,
    pub manotify: Option<Manotify>,
}
#[derive(Deserialize, Serialize)]
pub struct Manotify {
    pub last_id: u64,
}
