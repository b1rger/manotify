extern crate mammut;
extern crate toml;

use std::{
    error::Error,
    io,
};

use self::mammut::{
    apps::{
        AppBuilder,
        Scopes
    },
    Mastodon,
    Registration
};

pub fn register() -> Result<Mastodon, Box<Error>> {
    let app = AppBuilder {
        client_name: "manotify",
        redirect_uris: "urn:ietf:wg:oauth:2.0:oob",
        scopes: Scopes::All,
        website: Some("https://gitlab.com/bisco/manotify"),
    };

    let website = read_line("Please enter your mastodon instance url:")?;
    let mut registration = Registration::new(website.trim());
    registration.register(app)?;
    let url = registration.authorise()?;

    println!("Click this link to authorize on Mastodon: {}", url);
    let input = read_line("Paste the returned authorization code: ")?;

    let code = input.trim();
    let mastodon = registration.create_access_token(code.to_string())?;

    Ok(mastodon)
}

pub fn read_line(message: &str) -> Result<String, Box<Error>> {
    println!("{}", message);

    let mut input = String::new();
    io::stdin().read_line(&mut input)?;

    Ok(input)
}
